#!/bin/bash

echo "登录docker服务器"
docker login --username=pethos-admin@1591023460047980 registry.cn-beijing.aliyuncs.com --password=pethos2019

if [ $? -eq 0 ]
then
  echo "成功登录docker服务器"
else
  echo "登录docker服务器 不成功" >&2
  exit 1
fi


echo "暂停所有服务"
docker-compose down

if [ $? -eq 0 ]
then
  echo "成功暂停所有服务"
else
  echo "暂停所有服务 不成功" >&2
  exit 1
fi

printf "下载MySQL数据库image"
docker-compose pull his-db

if [ $? -eq 0 ]
then
  echo "成功下载MySQL数据库image"
else
  echo "下载MySQL数据库image 不成功" >&2
  exit 1
fi

printf "运行MySQL数据库容器"
docker-compose run -d his-db

if [ $? -eq 0 ]
then
  echo "成功运行MySQL数据库容器"
else
  echo "运行MySQL数据库容器 不成功" >&2
  exit 1
fi

printf "等待30秒,直到MySQL数据库容器运行稳定"
sleep 30

printf "设置文件执行权限"
chmod u+x ./run-services.sh
chmod u+x ./update-services.sh
chmod u+x ../his-backup/*.sh

if [ $? -eq 0 ]
then
  echo "成功运设置文件执行权限"
else
  echo "设置文件执行权限 不成功" >&2
  exit 1
fi

printf "初始化数据库"
../his-backup/restore.sh . true true false

if [ $? -eq 0 ]
then
  echo "成功初始化数据库"
else
  echo "初始化数据库 不成功" >&2
  exit 1
fi

printf "下载其他服务image"
docker-compose pull


if [ $? -eq 0 ]
then
  echo "成功下载其他服务image"
else
  echo "下载其他服务image 不成功" >&2
  exit 1
fi


echo "初始化完毕，接下来可执行 run-services.sh开始运行服务"
echo "请勿重复执行install.sh, 此安装脚本将重置数据库，除非数据库已备份，则请勿再次执行此脚本"


