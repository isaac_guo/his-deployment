# 配置环境
1. 安装操作系统 Ubuntu 18.04 LTS 桌面版 （或更高版本）
   - 点击连接https://ubuntu.com/download/desktop， 下载Ubuntu操作系系统iso文件。
   - 为了能够将Ubuntu操作系统安装到服务器（Server）上。 通常需要先制作USB(U盘）自启动安装盘。 具体制作过程，可以参考 https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-windows#0
   - 当自启动U盘制作完毕后，就可以将U盘插入Server上的USB口中，启动/重启Server，其启动最开始阶段，确定跳入选择启动项菜单（通常可能按键有F2，F10，F12，不同硬件，有不同的设置，需要依次尝试，或者查看Server手册）
   - 安装过程是图形化界面，按照画面提示，进行安装即可。
   - 安装过程需要设置管理员密码以及普通用户名和密码。请在设置完毕后，妥善保管好用户名与密码。
2. 安装Docker Community (docker社区版)
   - 具体安装流程可以访问https://docs.docker.com/install/linux/docker-ce/ubuntu/， 这里贴出简要流程
   - 在Ubuntu桌面环境下，使用热键 `alt+shift+t` 从而打开terminal。 
   - 在terminal里，逐条依次执行下述命令
   ```
    sudo apt-get remove docker docker-engine docker.io containerd runc
    sudo apt-get update
    sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        gnupg-agent \
        software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"
    sudo apt-get update
    sudo apt-get install docker-ce docker-ce-cli containerd.io
    sudo usermod -aG docker <your-user-account> 
    ```
   - **请将上述<your-user-account>替换为实际的用户名（在安装Ubuntu时创建的用户名）**
   - 安装完毕后，使用下面命令来测试docker 
   ```
    sudo docker run hello-world
   ```

   - 这条命令执行完毕后，应当会打印一条信息
   - 关闭当前terminal，重启系统
   
3. 安装docker-compose
   - 具体安装过程可参考https://docs.docker.com/compose/install/， 这里仅仅简要列出安装命令
   - 在Ubuntu桌面环境下，使用热键 `alt+shift+t` 从而打开terminal。 
   - 在terminal里，逐条依次执行下述命令
   ```
   sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   sudo chmod +x /usr/local/bin/docker-compose
   ```
   - 安装完毕后，可以使用下述命令进行测试
   
   ```
    docker-compose --version
   ```
    若安装成功，应打印信息类似如下：
    `docker-compose version 1.24.1, build 1110ad01`
   - 关闭当前terminal
   - 重新启动服务器
   - 重启回到桌面后, docker 需要login，才能有权限下载docker images, 打开terminal，执行下列命令
   ```
    docker login --username=pethos-admin@1591023460047980 registry.cn-beijing.aliyuncs.com
   ```
   然后，terminal弹出`Password:`
   - 输入秘密，回车
   - 若成功，则terminal打印出`Login Succeeded`字样
   - 若不成功，则需要确定密码是否正确。
   
   
4. 安装git
   - 默认情况下，git应当已经预装在Ubuntu系统中
   - 若在terminal下键入
     `git`
     没有打印出相应的git帮助信息，则需要安装，否则可以跳过此步。
     安装命令如下:
    ```
     sudo apt install git-all
    ```  

# 运行宠物医院管理系统
1. 确保网络畅通

2. 键入下述命令，从而首先克隆(clone) his-deployment项目至当前文件夹
   ```
   git clone https://gitlab.com/isaac_guo/his-deployment.git
   cd his-deployment
   ```
3. 修改数据库的备份数据的存储位置
   - 在Ubuntu桌面，在任务栏中点击**显示应用程序图标**（9点矩形图标），打开应用程序列表视图
   - 使用键盘敲入‘texteditor’,打开Text Editor（类似于windows下的写字板应用程序）
   - 在Text Editor中打开刚刚克隆的his-deployment/his-root/docker-compose.yml文件
   - 在143行， 会看到如下此行：
   ```
     "/root/his-backup:/his-backup"
   ```
   - 修改**/root/his-backup**为实际指定的位置，例如制定将数据库备份文件存储到/home/pethos/his-backup/文件夹
   ```
     "/home/pethos/his-backup:/his-backup"
   ```
4. 保存并退出
5. 打开terminal
6. 进入到his-deployment下的his-root文件夹（**比如，his-deployment存储在/home/pethos文件夹下**）
   ```
   cd /home/pethos/his-deployment/his-root   
   chmod u+x ./run-services.sh
   ```
7. 敲入下述命令，进行**首次安装**.
   `注意：执行下述命令将会初始化重置数据库。请注意！`
   ```
   chmod u+x ./install.sh
   ./install.sh
   ```
8. 第一次安装数据库之后，再敲入下述命令，运行宠物医院系统
   ```
   ./run-services.sh
   ```
9. 稍等1-2分钟，等待所有服务启动并开始运行后，打开浏览器（Ubuntu系统默认已经安装的浏览器为firefox),敲入
   ```
   http://localhost
   ```
   应当可以看到宠物医院的登录界面
   注：如果发现2分钟后无法访问网页，请回到terminal中，输入命令：
   ```
   docker ps -a
   ```
   如果发现，gateway服务运行失败，应当是服务器的80端口被占用，请查找80端口是否被占用，或者更改端口重新运行
   ```
   ./run-services.sh
   ```
 

# 运维
1. 保存公钥
- 打开terminal
- 请将本project下authorized_keys文件中的内容复制到/root/.ssh/authorized_keys下
- 如果/root/.ssh/authorized_keys不存在，则可所以直接将本project下的authorized_keys文件复制到/root/.ssh文件夹下

2. 手动备份数据库
- 打开terminal
- 进入到his-deployment下的his-root文件夹（**比如，his-deployment存储在/home/pethos文件夹下**）
   ```
   cd /home/pethos/his-deployment/his-back
   ```
- 执行下述命令
  ```
  ./backup.sh
  ```
- 此脚本将以当前时间为名创建文件夹，并且将数据库中的数据导出至此新建文件夹中, 保存以.sql为扩展名的若干文件中


3. 手动恢复数据库
- 打开terminal
- 进入到his-deployment下的his-root文件夹（**比如，his-deployment存储在/home/pethos文件夹下**）
   ```
   cd /home/pethos/his-deployment/his-back
   ```
- 执行下述命令
  ```
  ./restore.sh ./<文件夹名称> true true true
  ```
- 此脚本将使用输入的<文件夹名称>里面存放的以.sql为扩展名的若干文件，并且将其导入至数据库中
- 后面的3个参数分别代表为:
   * 是否销毁数据库
   * 是否重建数据库
   * 是否从制定的文件夹中导回数据至数据库中

4. 下载更新服务image
- 打开terminal
- 进入到his-deployment下的his-root文件夹（**比如，his-deployment存储在/home/pethos文件夹下**）
   ```
   cd /home/pethos/his-deployment/his-root
   ```
- 执行下述命令
  ```
  ./update-services.sh
  ```
5. 挂载备份磁盘
   **注意，此命令仅可执行一次，不可执行多次。因为此命令会创建文件夹以及写入挂载点信息到/etc/fstab，若执行多次，将会写入多次挂载点信息到/etc/fstab， 若需要执行，则确认之前写入到/etc/fstab中的信息已经彻底删除完毕**
   **注意，此条命令需要sudo执行**
- 打开terminal
- 进入到his-deployment下的**util**文件夹（**比如，his-deployment存储在/home/pethos文件夹下**）
   ```
   cd /home/pethos/his-deployment/util
   ```
- 执行下述命令
  ```
  chmod u+x ./mount_backup_drive.sh
  sudo ./mount_backup_drive.sh
  ```
  
  若执行脚本正确无差错，应当可以在桌面上看到新挂载后的文件夹
  
  **注意，新的挂载点为 /media/pethos_backup**, 可以将此路径信息替换掉 `运行宠物医院管理系统`第3步中的`/root/his-backup` 部分。
  

